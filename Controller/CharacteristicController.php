<?php

namespace Tide\TimeTideBundle\Controller;

use Tide\TimeTideBundle\Helpers\CharacteristicHelper;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Tide\TimeTideBundle\Entity\Characteristic;
use function Symfony\Component\DependencyInjection\Loader\Configurator\param;

class CharacteristicController extends AbstractController
{
    /**
     * @var EventDispatcherInterface $eventDispatcher
     */
    private $eventDispatcher;

    /**
     * @var CharacteristicHelper $characteristicHelper
     */
    private $characteristicHelper;

    public function __construct(EventDispatcherInterface $eventDispatcher, CharacteristicHelper $characteristicHelper)
    {
        $this->eventDispatcher = $eventDispatcher;
        $this->characteristicHelper = $characteristicHelper;
    }

    /**
     * @Route(
     *     name="tide_characteristic_add",
     *     path="/api/characteristics",
     *     methods={"POST"}
     * )
     */
    public function addCharacteristic(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $content = $request->getContent();
        $params = json_decode($content, true);

        if (!isset($params['user']) || !isset($params['sensorType']) || !isset($params['data']))
            return $this->json(['message' => 'Parameters missing (user, sensorType, data)'], 400);

        $label = null;
        if(isset($params['label'])){
          $label = $params['label'];
        }

        $user = $em->getRepository('TimeTideBundle:User')->find($params['user']);
        if (!$user)
            return $this->json(['message' => 'User not found'], 404);

        $clock = $this->getUser();
        if(!$em->getRepository('TimeTideBundle:User')->isUserInClock($user, $clock))
            return $this->json(['message' => 'Clock is not able to modify this user'], 403);

        $characteristic = $this->characteristicHelper->addCharacteristic($user, $clock, $params['sensorType'], $params['data'], true, $label);

        return $this->json(
            [
                'id'=>$characteristic->getId(),
                'sensorType'=>$characteristic->getSensorType(),
                'data'=>$characteristic->getData(),
                'user'=>['id'=>$characteristic->getUser()->getId()]
            ]
            ,201);
    }

    /**
     * @Route(
     *     name="tide_characteristic_remove",
     *     path="/api/characteristics/{id}",
     *     methods={"DELETE"}
     * )
     */
    public function removeCharacteristic(Request $request, $id)
    {

        $em = $this->getDoctrine()->getManager();
        /**
         * @var Characteristic $characteristic
         */
        $characteristic = $em->getRepository('TimeTideBundle:Characteristic')->find($id);

        if(!$characteristic)
            return $this->json(['message' => 'Characteristic not found'], 404);

        $clock = $this->getUser();
        $user = $characteristic->getUser();

        if(!$em->getRepository('TimeTideBundle:User')->isUserInClock($user, $clock))
            return $this->json(['message' => 'Clock is not able to modify this user'], 403);

        $em->remove($characteristic);
        $em->flush();

        $user->setUpdatedAt(new \DateTime());
        $em->persist($user);

        $em->flush();

        return $this->json([], 204);
    }

    /**
     * @Route(
     *     name="tide_characteristic_saved_ack",
     *     path="/api/characteristics/saved",
     *     methods={"PUT"}
     * )
     */
    public function savedAckAction(Request $request)
    {
        $content = $request->getContent();
        $params = json_decode($content, true);
        if (!isset($params['user']))
            return $this->json(['message' => 'Missing parameters (user)'], 400);
        $em = $this->getDoctrine()->getManager();
        $user = $em->find('TimeTideBundle:User', $params['user']);
        if (!$user)
            return $this->json(['message' => 'User not found']);
        $clock = $em->find('TimeTideBundle:Clock', $this->getUser());
        if (!$clock)
            return $this->json(['message' => 'Clock not found']);
        $clockUser = $em->getRepository('TimeTideBundle:ClockUser')->findOneBy(['user' => $user, 'clock' => $clock]);
        if (!$clockUser)
            return $this->json(['message' => 'User not assigned to clock']);
        $clockUser->setSyncDate(new \DateTime());
        $em->persist($clockUser);
        $em->flush();
        return $this->json([], 200);
    }

}