<?php
namespace Tide\TimeTideBundle\Controller;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Tide\TimeTideBundle\Entity\Clock;
use Tide\TimeTideBundle\Entity\LogEntry;

class LogEntryController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route(
     *     name="tide_create_log_entry",
     *     path="/api/log_entries",
     *     methods={"POST"}
     * )
     */
    public function createLogEntry(Request $request)
    {
        $content = $request->getContent();
        $params = json_decode($content, true);

        if(!$params['timestamp'])
            return $this->json('Timestamp not received.',400);

        if(!$params['detail']) {
            return $this->json('Detail not received.', 400);
        }

        /**
         * @var Clock $clock
         */
        $clock = $this->getUser();

        $logEntry = new LogEntry();
        $logEntry->setClock($clock);
        $logEntry->setDetail($params['detail']);
        $logEntry->setLoggedAt(\DateTime::createFromFormat('U', $params['timestamp']));
        $logEntry->setCreatedAt(new \DateTime());

        $this->em->persist($logEntry);
        $this->em->flush();

        //Return an empty response
        return $this->json([], 201);
    }

}