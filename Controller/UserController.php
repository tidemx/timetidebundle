<?php

namespace Tide\TimeTideBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Tide\TimeTideBundle\Entity\Clock;
use Tide\TimeTideBundle\Entity\User;
use Tide\TimeTideBundle\Helpers\TimeTideUserImageProviderInterface;

class UserController extends AbstractController {

    /**
     * @var TimeTideUserImageProviderInterface $imageProvider
     */
    private $imageProvider;

    /**
     * @var EntityManagerInterface $em
     */
    private $em;

    public function __construct( TimeTideUserImageProviderInterface $imageProvider, EntityManagerInterface $em ) {
        $this->em            = $em;
        $this->imageProvider = $imageProvider;
    }

    /**
     * @Route(
     *     name="tide_user_change_password",
     *     path="/api/users/change_password/{userId}",
     *     methods={"PUT"}
     * )
     */
    public function changePassword(Request $request, $userId){
        $userRepo = $this->em->getRepository( 'TimeTideBundle:User' );

        $user = $userRepo->find($userId);

        $content = $request->getContent();
        $params = json_decode($content, true);

        if(!$user)
            return $this->json(['error'=>'Not found', 'message'=>'User not found'], 404);

        if(!isset($params['password']))
            return $this->json(['error'=>'Bad request', 'message'=>'Password not received'], 400);

        $password = $params['password'];

        $user->setClockPassword($password);
        $user->setUpdatedAt(new \DateTime());

        $this->em->persist($user);
        $this->em->flush();

        return $this->json([], 204);

    }

    /**
     * @Route(
     *     name="tide_user_download_profile_image",
     *     path="/api/users/download_profile_image/{userId}",
     *     methods={"GET"}
     * )
     */
    public function downloadProfileImage( $userId ) {

        $userRepo = $this->em->getRepository( 'TimeTideBundle:User' );

        $user = $userRepo->find($userId);

        if(!$user)
            return new Response( 'User not found', 404 );

        /**
         * @var Clock $clock
         */
        $clock = $this->getUser();

        if ( ! $userRepo->isUserInclock( $user, $clock ) ) {
            return new Response( 'User not assigned to clock', 403 );
        }

        $image = $this->imageProvider->getProfileImage( $user );

        if(!$image)
            return new Response( 'Profile Image not found', 404 );

        return $this->file( $image );
    }
}