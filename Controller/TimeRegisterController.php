<?php
namespace Tide\TimeTideBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Tide\TimeTideBundle\Entity\Clock;
use Tide\TimeTideBundle\Entity\TimeRegister;
use Tide\TimeTideBundle\Entity\User;
use Tide\TimeTideBundle\Event\ClockPreResponseEvent;
use Tide\TimeTideBundle\Event\TideTimeRegistersCompletedEvent;

class TimeRegisterController extends AbstractController
{

    /**
     * @var EventDispatcherInterface $eventDispatcher
     */
    private $eventDispatcher;

    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @Route(
     *     name="tide_time_registers_add",
     *     path="/api/time_registers",
     *     methods={"POST"}
     * )
     */
    public function addRegisters(Request $request){

        $content = $request->getContent();
        $params = json_decode($content, true);

        $em = $this->getDoctrine()->getManager();
        /**
         * @var Clock $clock
         */
        $clock = $this->getUser();

        if(!$params['timeRegisters'])
            return $this->json('TimeRegisters not received.',400);

        $timeRegisters = [];
        foreach ($params['timeRegisters'] as $timeRegister){
            /** @var User $user*/
            $user = $em->find('TimeTideBundle:User', $timeRegister['user']);
            if(!$user)
                continue;

            $date = new \DateTime($timeRegister['date']);

            //check If timeRegister exists
            if(!$tr = $em->getRepository('TimeTideBundle:TimeRegister')->findOneBy(['user'=>$user, 'date'=>$date])){
                $tr = new TimeRegister();
                $tr->setDate($date);
                $tr->setClock($clock);
                $tr->setUser($user);
				if(isset($timeRegister['metadata'])){
					$tr->setMetadata($timeRegister['metadata']);
				}
                $em->persist($tr);
            }

            $timeRegisters[] = $tr;
        }

        $clock->setTimeRegistersSyncDate(new \DateTime());
        $em->persist($clock);
        $em->flush();

        // Added dispatch to new event for OSMOS V3
        $timeRegistersEvent = new TideTimeRegistersCompletedEvent($timeRegisters);
        $this->eventDispatcher->dispatch($timeRegistersEvent, TideTimeRegistersCompletedEvent::NAME);

        if(count($timeRegisters) === 1) {
            try {
                $event = new ClockPreResponseEvent($timeRegisters[0]);
                $this->eventDispatcher->dispatch( $event, ClockPreResponseEvent::NAME);
                $response = $event->getResponseData();
                return $this->json($response, 200);
            } catch (\Exception $exception) {
                return $this->json(['error'=>'Error getting additional data for response', 'message'=>$exception->getMessage()], 200);
            }
        }

        return $this->json([], 200);
    }
}