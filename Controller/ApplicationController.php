<?php

namespace Tide\TimeTideBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Serializer\SerializerInterface;
use Tide\TimeTideBundle\Entity\Clock;
use Tide\TimeTideBundle\Event\PluginDataReceivedEvent;
use Tide\TimeTideBundle\Helpers\ApplicationManager;
use Tide\TimeTideBundle\Helpers\ClockManager;

class ApplicationController extends AbstractController
{

    /**
     * @var ApplicationManager $applicationManager
     */
    private $applicationManager;

    /**
     * @var ClockManager $clockManager
     */
    private $clockManager;

    /**
     * @var EntityManagerInterface $em
     */
    private $em;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var EventDispatcherInterface $eventDispatcher
     */
    private $eventDispatcher;

    public function __construct(ApplicationManager $applicationManager, ClockManager $clockManager, EntityManagerInterface $em, SerializerInterface $serializer, EventDispatcherInterface $eventDispatcher)
    {
        $this->applicationManager = $applicationManager;
        $this->clockManager = $clockManager;
        $this->em = $em;
        $this->serializer = $serializer;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @Route(
     *     name="tide_releases",
     *     path="/api/releases",
     *     methods={"GET"}
     * )
     */
    public function list(Request $request)
    {
       $type = $request->query->get('type');

        /**
         * @var Clock $clock
         */
        $clock = $this->getUser();
        $applications = $this->clockManager->getClockApplications($clock, $type);

        return $this->json(['applications' => $applications]);
    }

    /**
     * @Route(
     *     name="tide_send_plugin_data",
     *     path="/api/plugins/send_data",
     *     methods={"POST"}
     * )
     */
    public function sendPluginData(Request $request)
    {
        /**
         * @var Clock $clock
         */
        $clock = $this->getUser();
        $data = json_decode($request->getContent(), true);

        if(!$data) {
            throw new \InvalidArgumentException('Bad request, possible bad json syntax');
        }

        $event = new PluginDataReceivedEvent($clock, $data);
        $this->eventDispatcher->dispatch($event, PluginDataReceivedEvent::NAME);

        return $this->json($event->getResponseData(), 200);
    }

    /**
     * @Route(
     *     name="tide_releases_download",
     *     path="/api/releases/{releaseId}/download",
     *     methods={"GET"}
     * )
     */
    public function downloadRelease($releaseId)
    {
        $release = $this->em->find('TimeTideBundle:Release', $releaseId);

        if (!$release) return new Response('Release not found', 404);

        $releaseFilePath = $this->applicationManager->getReleaseFilePath($release);
        if (!file_exists($releaseFilePath)) {
            return new Response('Installation file not found');
        }
        return $this->file($releaseFilePath);
    }

}