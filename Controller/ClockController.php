<?php

namespace Tide\TimeTideBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Tide\TimeTideBundle\Entity\Clock;
use Tide\TimeTideBundle\Exception\ClockLinkedException;
use Tide\TimeTideBundle\Helpers\ClockManager;

class ClockController extends AbstractController
{

    /**
     * @var ClockManager $clockManager
     */
    private $clockManager;

    /**
     * ClockController constructor.
     *
     * @param ClockManager $clockManager
     */
    public function __construct(ClockManager $clockManager)
    {
        $this->clockManager = $clockManager;
    }

    /**
     * @Route(
     *     name="tide_clock_get_users",
     *     path="/api/clock/users",
     *     methods={"GET"}
     * )
     */
    public function listUsers()
    {
        $em = $this->getDoctrine()->getManager();
        $clock = $this->getUser();
        $users = $this->clockManager->getClockUsers($clock);
        $clock->setUsersSyncDate(new \DateTime());
        $em->persist($clock);
        $em->flush();

        return $this->json($users, 200);
    }

    /**
     * @Route(
     *     name="tide_clock_connect",
     *     path="/api/clock/connect",
     *     methods={"PUT"}
     * )
     */
    public function connect(Request $request)
    {
        $content = $request->getContent();
        $params = json_decode($content, true);
        /**
         * @var Clock $clock
         */
        $clock = $this->getUser();

        if (isset($params['temperature'])) {
            $clock->setTemperature($params['temperature']);
        }
        if (isset($params['ipAddresses'])) {
            $clock->setIpAddresses($params['ipAddresses']);
        }
        if (isset($params['disk'])) {
            $clock->setDiskAvailable($params['disk']['available']);
            $clock->setDiskFree($params['disk']['free']);
            $clock->setDiskTotal($params['disk']['total']);
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($clock);
        $em->flush();

        $applications = $this->clockManager->getClockApplications($clock);

        return $this->json(['applications' => $applications]);
    }

    /**
     * @Route(
     *     name="tide_clock_link",
     *     path="/clock/link",
     *     methods={"POST"}
     * )
     */
    public function link(Request $request)
    {
        $content = $request->getContent();
        $params = json_decode($content, true);

        if (!isset($params['serial'])) {
            return $this->json(['message' => 'Missing parameters (serial)'], 400);
        }

        $serial = $params['serial'];
        $clock = $this->clockManager->getClockBySerial($serial);

        if (!$clock) {
            return $this->json(['message' => 'Clock with serial '.$serial.' not found'], 404);
        }

        try {
            $apikey = $this->clockManager->link($clock);
            return $this->json(['apikey' => $apikey]);
        } catch (ClockLinkedException $clockLinkedException) {
            return $this->json(['message' => 'Clock already linked'], 400);
        }

    }

}