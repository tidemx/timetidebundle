<?php

namespace Tide\TimeTideBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface {

	public function getConfigTreeBuilder() {
        $treeBuilder = new TreeBuilder('time_tide');
        $rootNode    = \method_exists(TreeBuilder::class, 'getRootNode') ? $treeBuilder->getRootNode() : $treeBuilder->root('time_tide');

		$rootNode
			->children()
				->scalarNode( 'image_provider' )->isRequired()->end()
                ->scalarNode( 'application_upload_destination' )->defaultValue("%kernel.project_dir%/tide_applications")->end()
				->end()
			->end();


		return $treeBuilder;
	}
}