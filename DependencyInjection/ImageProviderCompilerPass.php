<?php
namespace Tide\TimeTideBundle\DependencyInjection;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class ImageProviderCompilerPass implements CompilerPassInterface
{
	public function process(ContainerBuilder $container)
	{
		// get the service name from the container
		$id = $container->getParameter('image_provider');
		// inject the cache service into the Comrade Reader
		$definition = $container->getDefinition('Tide\TimeTideBundle\Controller\UserController');
		$definition->replaceArgument(0, new Reference($id));
    }
}
