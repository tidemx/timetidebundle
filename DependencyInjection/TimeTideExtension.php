<?php

namespace Tide\TimeTideBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class TimeTideExtension extends Extension {
	public function load( array $configs, ContainerBuilder $container ) {

		$loader = new YamlFileLoader(
			$container,
			new FileLocator( __DIR__ . '/../Resources/config' )
		);
		$loader->load( 'services.yml' );
        //$loader->load( 'security.yml' );

		$configuration = new Configuration();
		$config = $this->processConfiguration( $configuration, $configs );
		$container->setParameter('image_provider', $config['image_provider']);
        $container->setParameter('application_upload_destination', $config['application_upload_destination']);

		//Como inyectar servicios: https://stackoverflow.com/questions/40459643/inject-a-symfony-service-specified-in-config-yml-into-a-bundles-service
		$definition = $container->getDefinition('Tide\TimeTideBundle\Controller\UserController');
		$definition->replaceArgument(0, $config['image_provider']);

        $definition = $container->getDefinition('Tide\TimeTideBundle\Helpers\ApplicationManager');
        $definition->replaceArgument(2, $config['application_upload_destination']);
	}
}