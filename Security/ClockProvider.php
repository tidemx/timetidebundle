<?php
namespace Tide\TimeTideBundle\Security;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Tide\TimeTideBundle\Entity\Clock;
use Tide\TimeTideBundle\Entity\User;

class ClockProvider implements UserProviderInterface
{
	/**
	 * @var EntityManagerInterface $em
	 */
	private $em;

	public function __construct(EntityManagerInterface $em) {
		$this->em = $em;
	}


    public function getUsernameForApiKey($apiKey)
    {
        // Look up the username based on the token in the database, via
        // an API call, or do something entirely different
        /**
         * @var Clock $user
         */
        if( $user = $this->em->getRepository('TimeTideBundle:Clock')->findOneBy(['apiKey'=>$apiKey]))
            return $user->getUsername();

        throw new UsernameNotFoundException();
    }


    public function getUserForApiKey($apiKey)
    {
        // Look up the username based on the token in the database, via
        // an API call, or do something entirely different
        /**
         * @var Clock $user
         */
        if( $user = $this->em->getRepository('TimeTideBundle:Clock')->findOneBy(['apiKey'=>$apiKey]))
            return $user;

        throw new UsernameNotFoundException();
    }


    /**
	 * @return UserInterface
	 *
	 * @throws UsernameNotFoundException if the user is not found
	 */
	public function loadUserByUsername($username)
	{
		if( $user = $this->em->getRepository('TimeTideBundle:Clock')->findOneBy(['serial'=>$username]))
			return $user;

		throw new UsernameNotFoundException();
	}

	/**
	 * @return UserInterface
	 */
	public function refreshUser(UserInterface $user)
	{
        // this is used for storing authentication in the session
        // but in this example, the token is sent in each request,
        // so authentication can be stateless. Throwing this exception
        // is proper to make things stateless
        throw new UnsupportedUserException();
	}

	public function supportsClass($class)
	{
		return Clock::class === $class;
	}
}