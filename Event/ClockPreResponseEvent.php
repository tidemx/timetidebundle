<?php
namespace Tide\TimeTideBundle\Event;
use Symfony\Contracts\EventDispatcher\Event;
use Tide\TimeTideBundle\Entity\TimeRegister;

/**
 * Class ClockPreResponseEvent
 * @package App\Event
 * Cuando una persona checa en el reloj y cuenta con conexicón a internet,
 * el bundle regresa una respuesta vacía al reloj, por medio de este evento es posible
 * agregar información a la respuesta, es útil para la implementación de plugins.
 *
 *
 */
class ClockPreResponseEvent extends Event {
    const NAME = 'time_tide.clock.pre_response';

    /**
     * @var TimeRegister $timeRegister
     */
    protected $timeRegister;

    /**
     * @var array
     */
    protected $response = [];

    public function __construct(TimeRegister $timeRegister) {
        $this->timeRegister = $timeRegister;
    }

    public function getTimeRegister(){
        return $this->timeRegister;
    }

    public function getResponseData():array {
        return $this->response;
    }

    public function setResponseData(array $response){
        $this->response = $response;
    }

}
