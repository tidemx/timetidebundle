<?php
namespace Tide\TimeTideBundle\Event;
use Symfony\Contracts\EventDispatcher\Event;
use Tide\TimeTideBundle\Entity\Characteristic;
use Tide\TimeTideBundle\Entity\Clock;

/**
 * Class CheckRegisteredEvent
 * @package App\Event
 */
class CharacteristicPrePersistEvent extends Event {
    const NAME = 'time_tide.characteristic.pre_persist';

    /**
     * @var Characteristic $characteristic
     */
    protected $characteristic;

    /**
     * @var Clock $clock
     */
    protected $clock;

    public function __construct(Characteristic $characteristic,  Clock $clock) {
        $this->characteristic = $characteristic;
        $this->clock = $clock;
    }

    public function getCharacteristic(){
        return $this->characteristic;
    }

    public function getUploadedByClock(){
        return $this->clock;
    }
}
