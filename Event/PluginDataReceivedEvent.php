<?php
namespace Tide\TimeTideBundle\Event;
use Symfony\Contracts\EventDispatcher\Event;
use Tide\TimeTideBundle\Entity\Clock;
use Tide\TimeTideBundle\Entity\TimeRegister;

/**
 * Class ClockPreResponseEvent
 * @package App\Event
 * El evento se dispara cueando el servidor recive inromación de un plugin
 *
 */
class PluginDataReceivedEvent extends Event {
    const NAME = 'time_tide.plugin.data_received';

    /**
     * @var Clock $clock
     */
    protected $clock;

    /**
     * @var array
     */
    protected $data = [];

    protected $responseData = [];

    public function __construct(Clock $clock, array $data) {
        $this->clock = $clock;
        $this->data = $data;
    }

    public function getClock(){
        return $this->clock;
    }

    public function getData():array {
        return $this->data;
    }

    public function setResponseData(array $responseData){
       $this ->responseData = $responseData;
    }

    public function getResponseData(){
       return $this->responseData;
    }

}
