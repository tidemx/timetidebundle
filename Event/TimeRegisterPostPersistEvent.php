<?php
namespace Tide\TimeTideBundle\Event;
use Symfony\Contracts\EventDispatcher\Event;
use Tide\TimeTideBundle\Entity\TimeRegister;

/**
 * Class CheckRegisteredEvent
 * @package App\Event
 */
class TimeRegisterPostPersistEvent extends Event{
	const NAME = 'time_tide.time_register.post_persist';

	/**
	 * @var TimeRegister $timeRegister
	 */
	protected $timeRegister;

	public function __construct(TimeRegister $timeRegister) {
		$this->timeRegister = $timeRegister;
	}

	public function getTimeRegister(){
		return $this->timeRegister;
	}
}
