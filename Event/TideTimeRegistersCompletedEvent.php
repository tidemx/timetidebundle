<?php

namespace Tide\TimeTideBundle\Event;

use Symfony\Contracts\EventDispatcher\Event;
use Tide\TimeTideBundle\Entity\TimeRegister;

class TideTimeRegistersCompletedEvent extends Event
{
    public const NAME = 'tide_time_registers.created';

    /**
     * @var TimeRegister[]
     */
    private array $tideTimeRegisters;

    public function __construct(array $tideTimeRegisters)
    {
        $this->tideTimeRegisters = $tideTimeRegisters;
    }

    /**
     * @return TimeRegister[]
     */
    public function getTideTimeRegisters(): array
    {
        return $this->tideTimeRegisters;
    }
}