<?php
namespace Tide\TimeTideBundle\Event;

final class Events
{
	/**
	 * Triggered before a time_register persist.
	 *
	 * @Event("Tide\TimeTideBundle\Event\Event")
	 */
	public const PRE_PERSIST_TIME_REGISTERS = 'time_tide.time_register.pre_persist';

	/**
	 * Triggered after a time_register persist
	 *
	 * @Event("Tide\TimeTideBundle\Event\Event")
	 */
	public const POST_PERSIST_TIME_REGISTERS = 'time_tide.time_register.post_persist';

    /**
     * Triggered before response sent to clock
     *
     * @Event("Tide\TimeTideBundle\Event\Event")
     */
    public const PRE_RESPONSE_TO_CLOCK = 'time_tide.clock.pre_response';

    /**
     * Triggered before a characteristic persist
     *
     * @Event("Tide\TimeTideBundle\Event\Event")
     */
    public const PRE_PERSIST_CHARACTERISTIC = 'time_tide.characteristic.pre_persist';

    /**
     * Triggered after a characteristic persist
     *
     * @Event("Tide\TimeTideBundle\Event\Event")
     */
    public const POST_PERSIST_CHARACTERISTIC = 'time_tide.characteristic.post_persist';

    /**
     * Triggered after clock sends plugin data to server
     *
     * @Event("Tide\TimeTideBundle\Event\Event")
     */
    public const PLUGIN_DATA_RECEIVED = "time_tide.plugin.data_received";

    /**
     * Triggered after all time_register persist
     *
     * @Event("Tide\TimeTideBundle\Event\Event")
     */
    public const COMPLETED_TIME_REGISTERS_CREATED = "tide_time_registers.created";
}