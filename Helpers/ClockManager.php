<?php

namespace Tide\TimeTideBundle\Helpers;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Tide\TimeTideBundle\Entity\Clock;
use Tide\TimeTideBundle\Entity\ClockApplication;
use Tide\TimeTideBundle\Entity\ClockUser;
use Tide\TimeTideBundle\Entity\Release;
use Tide\TimeTideBundle\Entity\User;
use Tide\TimeTideBundle\Exception\ClockLinkedException;

/**
 * Class ClockManager
 * @package Tide\TimeTideBundle\Helpers
 */
class ClockManager
{

    /** @var EntityManagerInterface $em */
    private $em;

    /**
     * @var UserPasswordEncoderInterface $encoder
     */
    private $encoder;

    /**
     * @var \Doctrine\Common\Persistence\ObjectRepository
     */
    private $clockUserRepo;

    /** @var \Doctrine\Common\Persistence\ObjectRepository */
    private $userRepo;

    function __construct(EntityManagerInterface $em, UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
        $this->em = $em;
        $this->clockUserRepo = $this->em->getRepository('TimeTideBundle:ClockUser');
        $this->userRepo = $this->em->getRepository('TimeTideBundle:User');
    }

    public function createClock($name, $serial)
    {
        $em = $this->em;

        $clock = new Clock();
        $clock->setName($name);
        $clock->setSerial($serial);

        $clock->setApiKey($this->generateApiKey());

        $em->persist($clock);
        $em->flush();

        return $clock;
    }

    /**
     * @param TideClockRegistrableInterface[] $registrableEntities
     * @param Clock $clock
     */
    public function addUsersToClock(Clock $clock, $registrableEntities, $withFlush = true): void
    {

        foreach ($registrableEntities as $entity) {
            if (!$entity instanceof TideClockRegistrableInterface) {
                throw new \InvalidArgumentException(get_class($entity) . ' must implement TideClockRegistrableInterface');
            }
            //Check if entity has a TimeTideUser
            if (!$user = $entity->getTimeTideUser()) {
                $user = new User();
                $user->setDisplayName($entity->getDisplayName());
                $user->setUsername($entity->getUsername());
                $this->em->persist($user);
                $entity->setTimeTideUser($user);
                $this->em->persist($entity);
            }

            //Check if user is already registered in clock
            if ($this->clockUserRepo->findOneBy(['clock' => $clock, 'user' => $user])) {
                continue;
            }

            $clockUser = new ClockUser();
            $clockUser->setUser($user);
            $clockUser->setClock($clock);
            $this->em->persist($clockUser);
        }

        if($withFlush)
            $this->em->flush();
    }

    /**
     * @param TideClockRegistrableInterface[] $registrableEntities
     * @param Clock $clock
     */
    public function removeUsersFromClock(Clock $clock, $registrableEntities, $withFlush = true): void
    {
        foreach ($registrableEntities as $entity) {
            $user = $entity->getTimeTideUser();
            if ($clockUser = $this->clockUserRepo->findOneBy(['clock' => $clock, 'user' => $user])) {
                $this->em->remove($clockUser);
            }
        }

        if($withFlush)
            $this->em->flush();
    }

    /**
     * @param Clock $clock
     *
     * @return User[]
     */
    public function getClockUsers(Clock $clock)
    {
        return $this->userRepo->findUsersByClock($clock);
    }

    /**
     * @param $serial
     * @return object|Clock|null
     */
    public function getClockBySerial($serial)
    {
        return $this->em->getRepository('TimeTideBundle:Clock')->findOneBy(['serial' => $serial]);
    }

    /**
     * @param $id
     * @return object|Clock|null
     */
    public function getClockById($id){
        return $this->em->getRepository('TimeTideBundle:Clock')->find($id);
    }

    public function getClockApplications(Clock $clock, $type = null)
    {
        $clockApplications = $clock->getClockApplications();
        $applicationsArray = [];

        foreach ($clockApplications as $clockApplication) {
            if($type && $type !== $clockApplication->getApplication()->getType())
                continue;
            $applicationsArray[] = [
                'id' => $clockApplication->getRelease()->getId(),
                'name' => $clockApplication->getApplication()->getName(),
                'version' => $clockApplication->getRelease()->getVersion(),
                'type'=> $clockApplication->getApplication()->getType()
            ];
        }

        return $applicationsArray;
    }

    public function addApplicationToClock(Clock $clock, Release $release, $withFlush = true)
    {
        $clockApplication = $this->em->getRepository('TimeTideBundle:ClockApplication')->findOneBy(['clock' => $clock,
            'application' => $release->getApplication()
        ]);
        if (!$clockApplication) {
            $clockApplication = new ClockApplication();
            $clockApplication->setApplication($release->getApplication());
            $clockApplication->setClock($clock);
        }
        $clockApplication->setRelease($release);
        $this->em->persist($clockApplication);

        if($withFlush)
            $this->em->flush();
    }

    public function toggleProperty(Clock $clock, $property)
    {

        //Solo un reloj puede tener activo SSH y VNC a la vez
        $clocks = $this->em->getRepository('TimeTideBundle:Clock')->findAll();
        if ($property === 'ssh') {
            $isEnabled = $clock->getSsh();
        } elseif ($property === 'vnc') {
            $isEnabled = $clock->getVnc();
        }

        foreach ($clocks as $clockToDisable) {
            if ($property === 'ssh') {
                $clockToDisable->setSsh(false);
            } elseif ($property === 'vnc') {
                $clockToDisable->setVnc(false);
            }

            $this->em->persist($clockToDisable);
        }
        if (!$isEnabled) {
            if ($property === 'ssh') {
                $clock->setSsh(true);
            } elseif ($property === 'vnc') {
                $clock->setVnc(true);
            }
            $this->em->persist($clock);
        }
        $this->em->flush();
    }

    /**
     * @param Clock $clock
     * @return string
     * @throws ClockLinkedException
     */
    public function link(Clock $clock)
    {
        if ($clock->getLinkedDate()) {
            throw new ClockLinkedException();
        }
        $clock->setLinkedDate(new \DateTime());
        $this->em->persist($clock);
        $this->em->flush();
        return $clock->getApiKey();
    }

    public function resetApiKey(Clock $clock)
    {
        $clock->setApiKey($this->generateApiKey());
        $clock->setLinkedDate(null);
        $this->em->persist($clock);
        $this->em->flush();
    }

    public function generateApiKey()
    {
        return bin2hex(random_bytes(32)); // 64 characters long
    }


}