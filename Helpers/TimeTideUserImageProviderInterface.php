<?php
namespace Tide\TimeTideBundle\Helpers;

use Tide\TimeTideBundle\Entity\User;

interface TimeTideUserImageProviderInterface{

	function getProfileImage(User $user):?\SplFileObject;

}