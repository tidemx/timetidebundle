<?php

namespace Tide\TimeTideBundle\Helpers;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Tide\TimeTideBundle\Entity\Application;
use Tide\TimeTideBundle\Entity\Release;

/**
 * Class ApplicationManager
 * @package Tide\TimeTideBundle\Helpers
 */
class ApplicationManager {


	/**
	 * @var EntityManagerInterface
	 */
	private $em;

	/**
	 * @var ValidatorInterface
	 */
	private $validator;

	/**
	 * @var string
	 */
	private $uploadsFolder;

	/**
	 * @var string
	 */
	private $tmpFolder;

	public function __construct( EntityManagerInterface $em, ValidatorInterface $validator, $uploadsDir ) {
		$this->em            = $em;
		$this->validator     = $validator;
		$this->uploadsFolder = $uploadsDir;
		$this->tmpFolder     = $this->uploadsFolder . '/tmp';
	}

	/**
	 * @param string $name
	 *
	 * @return Application
	 */
	public function createApplication( string $name ) {
		$application = new Application();
		$application->setName( $name );

		$errors = $this->validator->validate( $application );

		if ( count( $errors ) > 0 ) {
			$errorsString = (string) $errors;
			throw new \InvalidArgumentException( $errorsString );
		}
		$this->em->persist( $application );
		$this->em->flush();

		return $application;
	}

	public function addRelease( $filePath ) {
		$this->createAppDirectories();


		//Copy zip to tmp applications directory
        $tmpFileName = 'tmp.zip';
		if ( ! copy( $filePath, $this->tmpFolder . '/'.$tmpFileName ) ) {
            $this->rrmdir( $this->tmpFolder );
			throw new \RuntimeException( sprintf( 'File "%s" was not copied', $filePath ) );
		}
		$errors = $this->extractAndValidateZip( $this->tmpFolder . '/'.$tmpFileName );
		if ( $errors && \count( $errors ) > 0 ) {
			$this->rrmdir( $this->tmpFolder );
			// throw new \InvalidArgumentException( implode( $errors, ', ' ) );
			throw new \InvalidArgumentException( implode( ', ', $errors ) );
		}

		$packageContent = file_get_contents( $this->tmpFolder . '/package.json' );
		$package        = json_decode( $packageContent, true );


		$application = $this->em->getRepository( 'TimeTideBundle:Application' )->findOneBy( [ 'name' => $package['name'] ] );
		$release     = null;

		if ( $application ) {
			$release = $this->em->getRepository( 'TimeTideBundle:Release' )->findOneBy( [
				'application' => $application,
				'version'     => $package['version']
			] );
		} else {
			$application = new Application();
			$application->setName( $package['name'] );
			$application->setType($package['type']);
			$this->em->persist( $application );
		}

		if ( ! $release ) {
			$release = new Release();
			$release->setApplication( $application );
			$release->setVersion( $package['version'] );
			$this->em->persist( $release );
		}

		rename( $this->tmpFolder . '/'.$tmpFileName, $this->uploadsFolder . '/' . $application->getName() . '-' . $release->getVersion() . '.zip' );
		$this->rrmdir( $this->tmpFolder );

		$this->em->flush();
		return $release;
	}

	public function removeApplication(Application $application){
		foreach ($application->getReleases() as $release){
			$this->removeRelease($release);
		}
		$this->em->remove($application);
		$this->em->flush();
	}

	public function removeRelease( Release $release ) {
		$this->em->remove( $release );
		$this->em->flush();
		$releaseFile = $this->getReleaseFilePath( $release );
		if ( file_exists( $releaseFile ) ) {
			unlink( $releaseFile );
		}
	}

	public function createAppDirectories() {

		if ( ! file_exists( $this->uploadsFolder ) ) {
			if ( ! mkdir( $this->uploadsFolder ) && ! is_dir( $this->uploadsFolder ) ) {
				throw new \RuntimeException( sprintf( 'Directory "%s" was not created', $this->uploadsFolder ) );
			}
		}

		if ( ! file_exists( $this->tmpFolder ) ) {
			if ( ! mkdir( $this->tmpFolder ) && ! is_dir( $this->tmpFolder ) ) {
				throw new \RuntimeException( sprintf( 'Directory "%s" was not created', $this->tmpFolder ) );
			}
		}
	}

	public function getReleaseFilePath( Release $release ) {
		return $this->uploadsFolder . '/' . $release->getApplication()->getName() . '-' . $release->getVersion() . '.zip';
	}

	public function extractAndValidateZip( $zipPath ) {
		$unzip  = new \ZipArchive;
		$out    = $unzip->open( $zipPath );
		$errors = [];
		if ( $out !== true ) {
			$errors[] = 'Zip can not be opened';

			return $errors;
		}

		if ( ! $unzip->extractTo( $this->tmpFolder ) ) {
			$errors[] = 'Zip can not be extracted';

			return $errors;
		}
		$unzip->close();

		if ( ! file_exists( $this->tmpFolder . '/package.json' ) ) {
			$errors[] = 'Couldn\'t find package . json';

			return $errors;
		}

		$packageContent = file_get_contents( $this->tmpFolder . '/package.json' );

		$package = json_decode( $packageContent, true );

		if ( $package === null || json_last_error() ) {
			$errors[] = 'Invalid package.json data';

			return $errors;
		}

		if ( ! isset( $package['name'] ) || ! isset( $package['version']) ||  ! isset( $package['type']) ) {
			$errors[] = 'The package.json file must contain name, version and type keys';

			return $errors;
		}

		if($package['type'] === Application::PLUGIN_TYPE && ! file_exists( $this->tmpFolder . '/index.html' )){
            $errors[] = 'Plugin index.html not found';

            return $errors;
        }

            //Check declared scrips exists and are executables
		if ( isset( $package['iotScripts'] ) && \count( $package['iotScripts'] ) > 0 ) {
			foreach ( $package['iotScripts'] as $script ) {
				if ( ! file_exists( $this->tmpFolder . '/' . $script['file'] ) ) {
					$errors[] = 'Could not find declared script ' . $script['file'];
				}
				/* validates file execution permissions, is not possible with ZipArchive, it should be extracted with unzip SO command https://github.com/composer/composer/issues/4471
					$permissions = fileperms($this->tmpFolder.'/package.json');
	                dump($permissions);
	                dump(substr(sprintf('%o', $permissions), -4));
	                //Last bit in 1 means file has execution permission
	                dump($permissions & 0x0001);
		         */
			}
		}
	}


	private function rrmdir( $dir ) {
		if ( is_dir( $dir ) ) {
			$objects = scandir( $dir );
			foreach ( $objects as $object ) {
				if ( $object != "." && $object != ".." ) {
					if ( is_dir( $dir . "/" . $object ) ) {
						$this->rrmdir( $dir . "/" . $object );
					} else {
						unlink( $dir . "/" . $object );
					}
				}
			}
			rmdir( $dir );
		}
	}


}
