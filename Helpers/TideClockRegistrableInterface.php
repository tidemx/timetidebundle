<?php
namespace Tide\TimeTideBundle\Helpers;

use Tide\TimeTideBundle\Entity\User;

/**
 * Interface ClockUserInterface
 * @package Tide\TimeTideBundle\Helpers
 */
interface TideClockRegistrableInterface{

	public function setTimeTideUser(User $user);

	public function getTimeTideUser() : ?User;

	public function getDisplayName() : string;

	public function getUsername() : string;

}

