<?php

namespace Tide\TimeTideBundle\Helpers;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Tide\TimeTideBundle\Entity\Characteristic;
use Tide\TimeTideBundle\Entity\Clock;
use Tide\TimeTideBundle\Entity\User;
use Tide\TimeTideBundle\Event\CharacteristicPostPersistEvent;
use Tide\TimeTideBundle\Event\CharacteristicPrePersistEvent;
use Tide\TimeTideBundle\Event\Events;

class CharacteristicHelper
{

    /**
     * @var EntityManagerInterface $em
     */
    private $em;

    /**
     * @var EventDispatcherInterface $eventDispatcher
     */
    private $eventDispatcher;

    public function __construct(EntityManagerInterface $em, EventDispatcherInterface $eventDispatcher)
    {
        $this->em = $em;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function addCharacteristic(User $user, ?Clock $clock, $sensorType, $data, $triggerEvents = true, $label = null)
    {

        //Older time tide versions
        if (!$label) {
            if (!$characteristic = $this->em->getRepository('TimeTideBundle:Characteristic')->findOneBy(['user' => $user,
                    'sensorType' => $sensorType]
            )) {
                $characteristic = new Characteristic();
                $characteristic->setUser($user);
                $characteristic->setSensorType($sensorType);
            }
        } else {
            $characteristic = new Characteristic();
            $characteristic->setUser($user);
            $characteristic->setSensorType($sensorType);
            $characteristic->setLabel($label);
        }

        $characteristic->setData($data);
        $characteristic->setRegisteredByClock($clock);

        if ($triggerEvents) {
            $prePersistEvent = new CharacteristicPrePersistEvent($characteristic, $clock);
            $this->eventDispatcher->dispatch( $prePersistEvent, Events::PRE_PERSIST_CHARACTERISTIC);
        }

        $this->em->persist($characteristic);

        $user->setUpdatedAt(new \DateTime());
        $this->em->persist($user);

        $this->em->flush();

        if ($triggerEvents) {
            $postPersistEvent = new CharacteristicPostPersistEvent($characteristic, $clock);
            $this->eventDispatcher->dispatch($postPersistEvent, Events::POST_PERSIST_CHARACTERISTIC);
        }

        return $characteristic;


    }
}