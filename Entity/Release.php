<?php

namespace Tide\TimeTideBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Tide\TimeTideBundle\Entity\Traits\TrackableTrait;
use Doctrine\ORM\Mapping\UniqueConstraint;
/**
 * Class Release
 * @package Tide\TimeTideBundle\Entity
 * @ORM\Entity(repositoryClass="Tide\TimeTideBundle\Repository\ReleaseRepository")
 * @ORM\Table(name="tide_release", uniqueConstraints={
 *   @UniqueConstraint(name="application_version_unique", columns={"application_id", "version"})
 * })
 * @UniqueEntity(fields={"application", "version"})
 * @ORM\HasLifecycleCallbacks()
 */
class Release {

	use TrackableTrait;

	/**
	 * @var integer
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue()
	 * @ORM\Id()
	 */
	private $id;

	/**
	 * @var Application
	 * @ORM\ManyToOne(targetEntity="Tide\TimeTideBundle\Entity\Application", inversedBy="releases")
	 * @Assert\NotNull()
	 * @ORM\JoinColumn(nullable=false)
	 */
	private $application;

	/**
	 * @var string
	 * @ORM\Column(type="string")
	 * @Assert\NotBlank()
	 */
	private $version;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getVersion(): ?string
    {
        return $this->version;
    }

    public function setVersion(string $version): self
    {
        $this->version = $version;

        return $this;
    }

    public function getApplication(): Application
    {
        return $this->application;
    }

    public function setApplication(Application $application): self
    {
        $this->application = $application;

        return $this;
    }

}
