<?php
namespace Tide\TimeTideBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use phpDocumentor\Reflection\Types\String_;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Tide\TimeTideBundle\Entity\Traits\TrackableTrait;

/**
 * Class Application
 * @package Tide\TimeTideBundle\Enity
 * @ORM\Table(name="tide_log_entry")
 * @ORM\Entity()
 */
class LogEntry{

    /**
     * @var integer
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue()
     * @ORM\Id()
     */
    private $id;

    /**
     * @var Clock
     * @ORM\ManyToOne(targetEntity="Tide\TimeTideBundle\Entity\Clock", inversedBy="clockApplications")
     * @ORM\JoinColumn(nullable=false)
     */
    private $clock;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=false)
     */
    private $detail;

    /**
     * @var \DateTime $createdAt
     *
     * @ORM\Column(type="datetime")
     */
    private $loggedAt;

    /**
     * @var \DateTime $createdAt
     *
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getClock(): Clock
    {
        return $this->clock;
    }

    public function setClock(Clock $clock): void
    {
        $this->clock = $clock;
    }

    public function getDetail(): string
    {
        return $this->detail;
    }

    public function setDetail(string $detail): void
    {
        $this->detail = $detail;
    }

    public function getLoggedAt(): \DateTime
    {
        return $this->loggedAt;
    }

    public function setLoggedAt(\DateTime $loggedAt): void
    {
        $this->loggedAt = $loggedAt;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }


}
