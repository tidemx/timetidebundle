<?php
namespace Tide\TimeTideBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Table(name="tide_time_register")
 * @ORM\Entity()
 */
class TimeRegister{

	/**
	 * @var int
	 * @ORM\Id
     * @Groups({"time_register_read"})
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @var \DateTime
     * @Groups({"time_register_read"})
	 * @ORM\Column(type="datetime")
	 */
	private $date;

	/**
	 * @var Clock
     * @Groups({"time_register_read"})
	 * @ORM\ManyToOne(targetEntity="Tide\TimeTideBundle\Entity\Clock", inversedBy="timeRegisters")
	 * @ORM\JoinColumn(nullable=false)
	 */
	private $clock;

	/**
	 * @var bool
     * @Groups({"time_register_read"})
	 * @ORM\Column(type="boolean", options={"default":1})
	 */
	private $isValid = true;

	/**
	 * @var User
	 * @ORM\ManyToOne(targetEntity="Tide\TimeTideBundle\Entity\User", inversedBy="timeRegisters")
	 * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
	 */
	private $user;

	/**
	 * @var string||null
	 * @Groups({"time_register_read"})
	 * @ORM\Column(type="text", nullable=true)
	 */
	private $metadata;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getIsValid(): ?bool
    {
        return $this->isValid;
    }

    public function setIsValid(bool $isValid): self
    {
        $this->isValid = $isValid;

        return $this;
    }

    public function getClock(): ?Clock
    {
        return $this->clock;
    }

    public function setClock(?Clock $clock): self
    {
        $this->clock = $clock;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

	public function getMetadata(): ?string
	{
		return $this->metadata;
	}

	public function setMetadata(?string $metadata): self
	{
		$this->metadata = $metadata;
		return $this;
	}

}