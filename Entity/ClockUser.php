<?php
namespace Tide\TimeTideBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Tide\TimeTideBundle\Entity\Traits\TrackableTrait;

/**
 * @ORM\Entity(repositoryClass="Tide\TimeTideBundle\Repository\ClockUserRepository")
 * @ORM\Table(name="tide_clock_user", uniqueConstraints={
 *   @UniqueConstraint(name="clock_user_unique", columns={"clock_id", "user_id"})
 * })
 * @ORM\HasLifecycleCallbacks()
 */
class ClockUser {

	use TrackableTrait;

	/**
	 * @var int
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @var Clock
	 * @ORM\ManyToOne(targetEntity="Tide\TimeTideBundle\Entity\Clock", inversedBy="clockUsers")
	 * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
	 */
	private $clock;

	/**
	 * @var User
	 * @ORM\ManyToOne(targetEntity="Tide\TimeTideBundle\Entity\User", inversedBy="clockUsers")
	 * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
	 */
	private $user;

	/**
	 * @var \DateTime
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	private $syncDate;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSyncDate(): ?\DateTimeInterface
    {
        return $this->syncDate;
    }

    public function setSyncDate(?\DateTimeInterface $syncDate): self
    {
        $this->syncDate = $syncDate;

        return $this;
    }

    public function getClock(): ?Clock
    {
        return $this->clock;
    }

    public function setClock(?Clock $clock): self
    {
        $this->clock = $clock;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

}