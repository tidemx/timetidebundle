<?php
namespace Tide\TimeTideBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Tide\TimeTideBundle\Entity\Traits\TrackableTrait;

/**
 * Class DeviceApplication
 * @package Tide\TimeTideBundle\Entity
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="tide_clock_application", uniqueConstraints={
 *   @UniqueConstraint(name="clock_application_unique", columns={"clock_id", "application_id"})
 * })
 * @UniqueEntity(fields={"clock", "application"})
 */
class ClockApplication{

	use TrackableTrait;

	/**
	 * @var integer
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue()
	 * @ORM\Id()
	 */
	private $id;

	/**
	 * @var Clock
	 * @ORM\ManyToOne(targetEntity="Tide\TimeTideBundle\Entity\Clock", inversedBy="clockApplications")
	 * @ORM\JoinColumn(nullable=false)
	 */
	private $clock;

	/**
	 * @var Application
	 * @ORM\ManyToOne(targetEntity="Tide\TimeTideBundle\Entity\Application")
	 * @ORM\JoinColumn(nullable=false)
	 */
	private $application;

	/**
	 * @var Release
	 * @ORM\ManyToOne(targetEntity="Tide\TimeTideBundle\Entity\Release")
	 * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
	 */
	private $release;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getClock(): ?Clock
    {
        return $this->clock;
    }

    public function setClock(?Clock $clock): self
    {
        $this->clock = $clock;

        return $this;
    }

    public function getApplication(): Application
    {
        return $this->application;
    }

    public function setApplication(Application $application): self
    {
        $this->application = $application;

        return $this;
    }

    public function getRelease(): Release
    {
        return $this->release;
    }

    public function setRelease(Release $release): self
    {
        $this->release = $release;

        return $this;
    }


}
