<?php
namespace Tide\TimeTideBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Tide\TimeTideBundle\Entity\Traits\TrackableTrait;

/**
 * @ORM\Table(name="tide_characteristic")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity()
 */
class Characteristic {

	use TrackableTrait;

	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @var User
	 * @Assert\NotNull()
	 * @ORM\ManyToOne(targetEntity="Tide\TimeTideBundle\Entity\User", inversedBy="characteristics")
	 * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
	 */
	private $user;

	/**
	 * @var string
	 * @Assert\Length(max="255")
	 * @ORM\Column(type="string", nullable=false)
	 */
	private $sensorType;

	/**
	 * @var string
	 * @Assert\Length(max="65000")
	 * @ORM\Column(type="text")
	 */
	private $data;

	/**
	 * @var Clock
	 * @Assert\NotNull()
	 * @ORM\ManyToOne(targetEntity="Tide\TimeTideBundle\Entity\Clock")
	 * @ORM\JoinColumn(nullable=true)
	 */
	private $registeredByClock;

    /**
     * @var string
     * @Assert\Length(max="255")
     * @ORM\Column(type="string", nullable=true)
     */
	private $label;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSensorType(): ?string
    {
        return $this->sensorType;
    }

    public function setSensorType(string $sensorType): self
    {
        $this->sensorType = $sensorType;

        return $this;
    }

    public function getData(): ?string
    {
        return $this->data;
    }

    public function setData(string $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getRegisteredByClock(): ?Clock
    {
        return $this->registeredByClock;
    }

    public function setRegisteredByClock(?Clock $registeredByClock): self
    {
        $this->registeredByClock = $registeredByClock;

        return $this;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(?string $label): self
    {
        $this->label = $label;
        return $this;
    }
}
