<?php
namespace Tide\TimeTideBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Tide\TimeTideBundle\Entity\Traits\TrackableTrait;

/**
 * Class Application
 * @package Tide\TimeTideBundle\Enity
 * @ORM\Table(name="tide_application")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity()
 * @UniqueEntity("name")
 */
class Application{

	use TrackableTrait;

	const PLUGIN_TYPE = 'plugin';

	/**
	 * @var integer
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue()
	 * @ORM\Id()
	 */
	private $id;

	/**
	 * @var string
	 * @ORM\Column(type="string", nullable=false, unique=true)
	 * @Assert\NotBlank()
	 */
	private $name;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     * @Assert\NotBlank()
     */
	private $type;

	/**
	 * @var Release[]
	 * @ORM\OrderBy({"version"="DESC"})
	 * @ORM\OneToMany(targetEntity="Tide\TimeTideBundle\Entity\Release", mappedBy="application", cascade={"remove"})
	 */
	private $releases;

    public function __construct()
    {
        $this->releases = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Release[]
     */
    public function getReleases(): Collection
    {
        return $this->releases;
    }

    public function addRelease(Release $release): self
    {
        if (!$this->releases->contains($release)) {
            $this->releases[] = $release;
            $release->setApplication($this);
        }

        return $this;
    }

    public function removeRelease(Release $release): self
    {
        if ($this->releases->contains($release)) {
            $this->releases->removeElement($release);
            // set the owning side to null (unless already changed)
            if ($release->getApplication() === $this) {
                $release->setApplication(null);
            }
        }

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return mixed|Release|null
     */
    public function getLastRelease(){
        if(count($this->releases) === 0 )
            return null;
        return $this->releases[0];
    }

}
