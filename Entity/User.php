<?php

namespace Tide\TimeTideBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Tide\TimeTideBundle\Entity\Traits\TrackableTrait;

/**
 * @ORM\Table(name="tide_user")
 * @ORM\Entity(repositoryClass="Tide\TimeTideBundle\Repository\UserRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class User {

	use TrackableTrait;

	/**
	 * @var int
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @var string
	 * @Assert\Length(max="255")
	 * @ORM\Column(type="string", length=255)
	 */
	private $displayName;

    /**
     * @var string
     * @Assert\Length(max="255")
     * @ORM\Column(type="string", length=255, nullable=true, unique=true)
     */
	private $username;

	/**
	 * @var TimeRegister[]
	 * @ORM\OneToMany(targetEntity="Tide\TimeTideBundle\Entity\TimeRegister", mappedBy="user", cascade={"remove"})
	 */
	private $timeRegisters;

	/**
	 * @var Characteristic[]
	 * @ORM\OneToMany(targetEntity="Tide\TimeTideBundle\Entity\Characteristic", mappedBy="user", cascade={"remove"})
	 */
	private $characteristics;

	/**
	 * @var ClockUser[]
	 * @ORM\OneToMany(targetEntity="Tide\TimeTideBundle\Entity\ClockUser", mappedBy="user", cascade={"remove"})
	 */
	private $clockUsers;

	/**
	 * @var string
	 * @ORM\Column(type="string", nullable=true)
	 */
	private $clockPassword;

	/**
	 * @var boolean
	 * @ORM\Column(type="boolean", nullable=true)
	 */
	private $isClockAdmin;

	/**
	 * @var \DateTime
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	private $profileImageUpdatedAt;

    /**
     * @var string||null
     * Should be a valid JSON
     * @ORM\Column(type="text", nullable=true)
     */
	private $metadata;

	public function __construct() {
      		$this->timeRegisters   = new ArrayCollection();
      		$this->characteristic  = new ArrayCollection();
      		$this->clockUsers      = new ArrayCollection();
      		$this->characteristics = new ArrayCollection();
      	}

	public function getId(): ?int {
      		return $this->id;
      	}

	public function getDisplayName(): ?string {
      		return $this->displayName;
      	}

	public function setDisplayName( string $displayName ): self {
      		$this->displayName = $displayName;
      
      		return $this;
      	}

	/**
	 * @return Collection|TimeRegister[]
	 */
	public function getTimeRegisters(): Collection {
      		return $this->timeRegisters;
      	}

	public function addTimeRegister( TimeRegister $timeRegister ): self {
      		if ( ! $this->timeRegisters->contains( $timeRegister ) ) {
      			$this->timeRegisters[] = $timeRegister;
      			$timeRegister->setUser( $this );
      		}
      
      		return $this;
      	}

	public function removeTimeRegister( TimeRegister $timeRegister ): self {
      		if ( $this->timeRegisters->contains( $timeRegister ) ) {
      			$this->timeRegisters->removeElement( $timeRegister );
      			// set the owning side to null (unless already changed)
      			if ( $timeRegister->getUser() === $this ) {
      				$timeRegister->setUser( null );
      			}
      		}
      
      		return $this;
      	}

	/**
	 * @return Collection|ClockUser[]
	 */
	public function getClockUsers(): Collection {
      		return $this->clockUsers;
      	}

	public function addClockUser( ClockUser $clockUser ): self {
      		if ( ! $this->clockUsers->contains( $clockUser ) ) {
      			$this->clockUsers[] = $clockUser;
      			$clockUser->setUser( $this );
      		}
      
      		return $this;
      	}

	public function removeClockUser( ClockUser $clockUser ): self {
      		if ( $this->clockUsers->contains( $clockUser ) ) {
      			$this->clockUsers->removeElement( $clockUser );
      			// set the owning side to null (unless already changed)
      			if ( $clockUser->getUser() === $this ) {
      				$clockUser->setUser( null );
      			}
      		}
      
      		return $this;
      	}

	/**
	 * @return Collection|Characteristic[]
	 */
	public function getCharacteristics(): Collection {
      		return $this->characteristics;
      	}

	public function addCharacteristic( Characteristic $characteristic ): self {
      		if ( ! $this->characteristics->contains( $characteristic ) ) {
      			$this->characteristics[] = $characteristic;
      			$characteristic->setUser( $this );
      		}
      
      		return $this;
      	}

	public function removeCharacteristic( Characteristic $characteristic ): self {
      		if ( $this->characteristics->contains( $characteristic ) ) {
      			$this->characteristics->removeElement( $characteristic );
      			// set the owning side to null (unless already changed)
      			if ( $characteristic->getUser() === $this ) {
      				$characteristic->setUser( null );
      			}
      		}
      
      		return $this;
      	}

	public function getClockPassword(): ?string {
      		return $this->clockPassword;
      	}

	public function setClockPassword( ?string $clockPassword ): self {
      		$this->clockPassword = $clockPassword;
      
      		return $this;
      	}

	public function getIsClockAdmin(): ?bool {
      		return $this->isClockAdmin;
      	}

	public function setIsClockAdmin( ?bool $isClockAdmin ): self {
      		$this->isClockAdmin = $isClockAdmin;
      
      		return $this;
      	}

	public function getProfileImageUpdatedAt(): ?\DateTimeInterface {
      		return $this->profileImageUpdatedAt;
      	}

	public function setProfileImageUpdatedAt( ?\DateTimeInterface $profileImageUpdatedAt ): self {
      		$this->profileImageUpdatedAt = $profileImageUpdatedAt;
      
      		return $this;
      	}

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(?string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @return string
     */
    public function getMetadata(): ?string
    {
        return $this->metadata;
    }

    /**
     * @param string $metadata
     */
    public function setMetadata(string $metadata): void
    {
        $this->metadata = $metadata;
    }



}