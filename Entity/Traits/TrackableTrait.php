<?php
namespace Tide\TimeTideBundle\Entity\Traits;

trait TrackableTrait{
	/**
	 * @var \DateTime $createdAt
	 *
	 * @ORM\Column(type="datetime")
	 */
	private $createdAt;

	/**
	 * @var \DateTime $updatedAt
	 *
	 * @ORM\Column(type="datetime")
	 */
	private $updatedAt;


	/**
	 * Get createdAt
	 *
	 * @return \DateTime
	 */
	public function getCreatedAt()
	{
		return $this->createdAt;
	}

	/**
	 * @param \DateTime $createdAt
	 *
	 * @return TrackableTrait
	 */
	public function setCreatedAt($createdAt)
	{
		$this->createdAt = $createdAt;
		return $this;
	}

	/**
	 * @return \Datetime
	 */
	public function getUpdatedAt()
	{
		return $this->updatedAt;
	}

	/**
	 * @param \Datetime $updatedAt
	 *
	 * @return TrackableTrait
	 */
	public function setUpdatedAt($updatedAt)
	{
		$this->updatedAt = $updatedAt;
		return $this;
	}

	/**
	 *
	 * @ORM\PrePersist
	 * @ORM\PreUpdate
	 */
	public function updatedTimestamps()
	{
		$this->setUpdatedAt(new \DateTime(date('Y-m-d H:i:s')));

		if($this->getCreatedAt() == null)
		{
			$this->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
		}
	}
}

