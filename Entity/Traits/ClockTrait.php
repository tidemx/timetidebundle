<?php
namespace Tide\TimeTideBundle\Entity\Traits;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Serializer\Annotation\Groups;
use Tide\TimeTideBundle\Entity\ClockApplication;
use Tide\TimeTideBundle\Entity\ClockUser;
use Tide\TimeTideBundle\Entity\TimeRegister;

trait ClockTrait
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @Groups({"time_register_read"})
     * @Assert\NotBlank()
     * @Assert\Length(max="255")
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(max="255")
     * @ORM\Column(type="string", unique=true)
     */
    private $serial;

    /**
     * @var string
     * @Assert\Length(max="500")
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $apiKey;

    /**
     * @var TimeRegister
     * @ORM\OneToMany(targetEntity="Tide\TimeTideBundle\Entity\TimeRegister", mappedBy="clock", cascade={"remove"})
     */
    private $timeRegisters;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $usersSyncDate;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $timeRegistersSyncDate;

    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $temperature;

    /**
     * @var array
     * @Assert\Ip()
     * @ORM\Column(type="array", nullable=true)
     */
    private $ipAddresses;

    /**
     * @var int
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $diskAvailable;

    /**
     * @var int
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $diskFree;

    /**
     * @var int
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $diskTotal;

    /**
     * @var ClockApplication[]
     * @ORM\OneToMany(targetEntity="Tide\TimeTideBundle\Entity\ClockApplication", mappedBy="clock", cascade={"remove"})
     */
    private $clockApplications;

    /**
     * @var ClockUser[]
     * @ORM\OneToMany(targetEntity="Tide\TimeTideBundle\Entity\ClockUser", mappedBy="clock", cascade={"remove"})
     */
    private $clockUsers;

    /**
     * @var \DateTime $databaseChangeDate
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $userDatabaseUpdatedAt;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $vnc = false;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $ssh = false;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $style;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $linkedDate;

    public function __construct()
    {
        $this->timeRegisters = new ArrayCollection();
        $this->clockApplications = new ArrayCollection();
        $this->clockUsers = new ArrayCollection();
    }

    public function getRoles()
    {
        return ['ROLE_TIME_TIDE'];
    }

    public function getPassword()
    {
        return '';
    }

    public function getSalt()
    {
        return null;
    }

    public function getUsername()
    {
        return $this->serial;
    }

    public function eraseCredentials()
    {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSerial(): ?string
    {
        return $this->serial;
    }

    public function setSerial(string $serial): self
    {
        $this->serial = $serial;

        return $this;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getUsersSyncDate(): ?\DateTimeInterface
    {
        return $this->usersSyncDate;
    }

    public function setUsersSyncDate(?\DateTimeInterface $usersSyncDate): self
    {
        $this->usersSyncDate = $usersSyncDate;

        return $this;
    }

    public function getTimeRegistersSyncDate(): ?\DateTimeInterface
    {
        return $this->timeRegistersSyncDate;
    }

    public function setTimeRegistersSyncDate(?\DateTimeInterface $timeRegistersSyncDate): self
    {
        $this->timeRegistersSyncDate = $timeRegistersSyncDate;

        return $this;
    }

    public function getTemperature(): ?float
    {
        return $this->temperature;
    }

    public function setTemperature(?float $temperature): self
    {
        $this->temperature = $temperature;

        return $this;
    }


    public function getDiskAvailable(): ?int
    {
        return $this->diskAvailable;
    }

    public function setDiskAvailable(?int $diskAvailable): self
    {
        $this->diskAvailable = $diskAvailable;

        return $this;
    }

    public function getDiskFree(): ?int
    {
        return $this->diskFree;
    }

    public function setDiskFree(?int $diskFree): self
    {
        $this->diskFree = $diskFree;

        return $this;
    }

    /**
     * @return Collection|TimeRegister[]
     */
    public function getTimeRegisters(): Collection
    {
        return $this->timeRegisters;
    }

    public function addTimeRegister(TimeRegister $timeRegister): self
    {
        if (!$this->timeRegisters->contains($timeRegister)) {
            $this->timeRegisters[] = $timeRegister;
            $timeRegister->setClock($this);
        }

        return $this;
    }

    public function removeTimeRegister(TimeRegister $timeRegister): self
    {
        if ($this->timeRegisters->contains($timeRegister)) {
            $this->timeRegisters->removeElement($timeRegister);
            // set the owning side to null (unless already changed)
            if ($timeRegister->getClock() === $this) {
                $timeRegister->setClock(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ClockApplication[]
     */
    public function getClockApplications(): Collection
    {
        return $this->clockApplications;
    }

    public function addClockApplication(ClockApplication $clockApplication): self
    {
        if (!$this->clockApplications->contains($clockApplication)) {
            $this->clockApplications[] = $clockApplication;
            $clockApplication->setClock($this);
        }

        return $this;
    }

    public function removeClockApplication(ClockApplication $clockApplication): self
    {
        if ($this->clockApplications->contains($clockApplication)) {
            $this->clockApplications->removeElement($clockApplication);
            // set the owning side to null (unless already changed)
            if ($clockApplication->getClock() === $this) {
                $clockApplication->setClock(null);
            }
        }

        return $this;
    }

    public function getIpAddresses(): ?array
    {
        return $this->ipAddresses;
    }

    public function setIpAddresses(?array $ipAddresses): self
    {
        $this->ipAddresses = $ipAddresses;

        return $this;
    }

    public function getUserDatabaseUpdatedAt(): ?\DateTimeInterface
    {
        return $this->userDatabaseUpdatedAt;
    }

    public function setUserDatabaseUpdatedAt(?\DateTimeInterface $userDatabaseUpdatedAt): self
    {
        $this->userDatabaseUpdatedAt = $userDatabaseUpdatedAt;

        return $this;
    }

    public function getDiskTotal(): ?int
    {
        return $this->diskTotal;
    }

    public function setDiskTotal(?int $diskTotal): self
    {
        $this->diskTotal = $diskTotal;

        return $this;
    }

    public function getVnc(): ?bool
    {
        return $this->vnc;
    }

    public function setVnc(bool $vnc): self
    {
        $this->vnc = $vnc;

        return $this;
    }

    public function getSsh(): ?bool
    {
        return $this->ssh;
    }

    public function setSsh(bool $ssh): self
    {
        $this->ssh = $ssh;

        return $this;
    }

    public function getStyle(): ?array
    {
        return $this->style;
    }

    public function setStyle(?array $style): self
    {
        $this->style = $style;

        return $this;
    }

    /**
     * @return Collection|ClockUser[]
     */
    public function getClockUsers(): Collection
    {
        return $this->clockUsers;
    }

    public function addClockUser(ClockUser $clockUser): self
    {
        if (!$this->clockUsers->contains($clockUser)) {
            $this->clockUsers[] = $clockUser;
            $clockUser->setClock($this);
        }

        return $this;
    }

    public function removeClockUser(ClockUser $clockUser): self
    {
        if ($this->clockUsers->contains($clockUser)) {
            $this->clockUsers->removeElement($clockUser);
            // set the owning side to null (unless already changed)
            if ($clockUser->getClock() === $this) {
                $clockUser->setClock(null);
            }
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getApiKey(): string
    {
        return $this->apiKey;
    }

    /**
     * @param string $apiKey
     */
    public function setApiKey(string $apiKey): void
    {
        $this->apiKey = $apiKey;
    }

    public function getLinkedDate(): ?\DateTimeInterface
    {
        return $this->linkedDate;
    }

    public function setLinkedDate(?\DateTimeInterface $linkedDate): self
    {
        $this->linkedDate = $linkedDate;

        return $this;
    }
}