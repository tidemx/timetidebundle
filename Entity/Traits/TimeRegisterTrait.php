<?php
namespace Tide\TimeTideBundle\Entity\Traits;
use Symfony\Component\Serializer\Annotation\Groups;
use Tide\TimeTideBundle\Entity\Clock;
use Tide\TimeTideBundle\Entity\User;

trait TimeRegisterTrait
{
    /**
     * @var int
     * @ORM\Id
     * @Groups({"time_register_read"})
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var \DateTime
     * @Groups({"time_register_read"})
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @var Clock
     * @Groups({"time_register_read"})
     * @ORM\ManyToOne(targetEntity="Tide\TimeTideBundle\Entity\Clock", inversedBy="timeRegisters")
     * @ORM\JoinColumn(nullable=false)
     */
    private $clock;

    /**
     * @var bool
     * @Groups({"time_register_read"})
     * @ORM\Column(type="boolean", options={"default":1})
     */
    private $isValid = true;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="Tide\TimeTideBundle\Entity\User", inversedBy="timeRegisters")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getIsValid(): ?bool
    {
        return $this->isValid;
    }

    public function setIsValid(bool $isValid): self
    {
        $this->isValid = $isValid;

        return $this;
    }

    public function getClock(): ?Clock
    {
        return $this->clock;
    }

    public function setClock(?Clock $clock): self
    {
        $this->clock = $clock;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}