Instalación
============


TimeTideBundle puede instalarse con ayuda de composer, no se encuentra publicado en 
packagist.org pero es posible instalarlo por medio de su repositorio de git y agregando 
una breve configuración al archivo composer.json. 

### Paso 1: Agregar repositorio de git a composer.json
```
...
    "repositories": [{
        "type": "vcs",
        "url": "https://gitlab.com/tidemx/TimeTideBundle"
    }],
...
    "require": "tide/time-tide-bundle": "dev-master"
...
```

### Paso 2: Actualizar composer 

```console
$ composer update
```

Habilitar bundle
----------------------------------------


### Aplicaciones no compatibles con symfony flex 
Agrega el bundle a la lista de bundles registrados en el archivo `app/AppKernel.php` de tu proyecto:

```php
<?php
// app/AppKernel.php

// ...
class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            // ...
            new Tide\TimeTideBundle\TimeTideBundle()
        );

        // ...
    }

    // ...
}
```

### Aplicaciones compatibles con symfony flex
Agrega el bundle a la lista de bundles registrados en el archivo `config/bundles.php` de tu proyecto:
```php
<?php
// config/bundles.php

return [
//...
    Tide\TimeTideBundle\TimeTideBundle::class => ['all' => true],
//...	
];
```

Configuración
----------------------------------------

### Rutas
TimeTideBundle incluye controladores y rutas que mantienen a los relojes sincronizados, agrega
las rutas modificando el archivo `config/routes.yaml`. Todas las rutas contienen el prefijo time_tide.
```yaml
time_tide_bundle:
  resource: "@TimeTideBundle/Controller"
  type: annotation
  prefix: /time_tide
  defaults:
    _format: json
 ```
 
 ### Profile image profiler
 Configuración de servicio utilizado para la obtención de imágenes.
 `config/packages/time_tide.yaml`
 ```yaml
 time_tide:
   image_provider: App\Helpers\TimeTideUserImageProvider
 ```
 
### Seguridad 
El acceso a los endpoints del bundle deben estar detrás de un firewall, el método de autenticación 
de los relojes es JWT. La documentación contempla que cuentas 
con el siguiente bundle instalado y configurado 
[`lexik/jwt-authentication-bundle`](https://packagist.org/packages/lexik/jwt-authentication-bundle).

#### Firewall
La entidad de reloj implementa la interfaz de usuario de symfony por lo que la configuración es similar
a la de cualquier usuario de symfony, los relojes se autentican por medio de un apikey que se genera al registrar el reloj,
 solo es necesario importar la configuración de seguridad en el archivo security.yml de la aplicación.
```
imports:
  - { resource: '@TimeTideBundle/Resources/config/security.yml' }

```

Comandos
----------------------------------------
### Registro de nuevo reloj
```
time-tide:register-clock {name} {serial}
```
Crea una entidad de reloj y le asigna una contraseña.


### Revisión de salud de relojes
```
time-tide:check-clock-health
```
Revisa el estado de los relojes y arroja una excepción en caso de detectar problemas
(Mucho tiempo desconectado, temperatura alta, almacenamiento excedido).

Eventos
----------------------------------------
Los eventos ofrecen una manera de engancharse a algún punto del flujo en el código.
Esta es una lista de los eventos que puedes escuchar:
 
| Nombre de evento | Punto de lanzamiento|
|------------|--------------|
|`time_tide.time_register.pre_persist`|Antes de que un registro sea persistido|
|`time_tide.time_register.post_persist`|Después de que un registro es persistido|  
|`time_tide.characteristic.pre_persist`|Antes de que una huella digital u otro método de indentificación sea persistido|
|`time_tide.characteristic.post_persist`|Después de que una huella digital u otro método de indentificación sea persistido|  
|`time_tide.clock.pre_response`|Antes de regresar una respuesta al reloj (En este evento deben agregarse a la respuesta el plugin que se tenga que activar en el reloj)|  
|`time_tide.plugin.data_received`|Al recivir información del reloj envíada por medio de un plugin|  



Arquitectura del reloj
----------------------------------------
El reloj cuenta con tres aplicaciones principales instaladas:

Time Tide: Encargada de almacenar y sincronizar huellas y 
registros de asistencia de los usuarios vinculados a ese reloj, también tiene el control
del hardware del dispositivo (Wifi, Sensores)  
 
Time Tide Front: Interfaz web desarrollada en ReactJS, se comunica directamente con Time Tide.

IOT: Se encarga de actualizar el software (Time Tide y Time Tide Front) en cuanto se detecta
que hay una nueva disponible, también crea los túneles SSH y VNC para poder realizar asistencia
remota. 

Diagrama de base de datos y explicación de entidades
----------------------------------------
![](./doc/img/diagrama_eer_time_tide_bunde.jpg)


El bundle es capaz de indicar que aplicaciones deben estar instaladas en el reloj, quién se encarga
de descargar las actualizaciones e instalarlas es el IOT de cada reloj. Las entidades asociadas son:

- Clock: Es la entidad principal del bundle, asocia un dispositivo físico con la aplicación, la propiedad 
serial es única por dispositivo y funciona como identificador.

- User: El bundle encapsula la funcionalidad por medio de esta entidad y la hace independiente
 de las entidades de lógica de negocio , los registros de asistencia e identidad biométrica se
  asocian a este usuario. Para poder vincular una entidad que represente a una persona 
  (Empleado, proveedor, etc), es necesario implementar la interfaz TideClockRegistrableInterface, 
  si estas entidades cuentan con imágenes de perfil se debe registrar el servicio 
  TimeTideUserImageProviderInterface que se describe más adelante. 
 

- ClockUser: No todos los User deben sincronizarse en todos los relojes, esto dependerá de la lógica
de negocio, se pueden agrupar los usuarios por empresa, sucursal, etc. Esta entidad define que usuario
puede registrarse en que reloj.

- TimeRegister: Es el registro de asistencia que se genera al identificar a una persona en el reloj.

- Application: Aplicaciones que pueden ser instaladas en los relojes.

- ClockApplication: Especifica que versión de aplicación debe estar instalada en los relojes.
 
- Release: Cada aplicación tiene varias versiones, se guarda en el servidor un zip con el 
código del release.

- Characteristic: Almacena la información de acceso de cada usuario, principalmente huella digital, también puede 
utilizarse para guardar información de IRIS o tarjeta RFID por ejemplo. 



Interfaz del bundle con usuarios de Osmos (Empleados, Administradores, etc)
----------------------------------------
El bundle no se comunica directamente con las entidades de la lógica de negocio, 
lo hace con ayuda de la entidad User.

Interfaz de vinculación de usuarios
--------------------------------------
Las entidades de usuario que se conectan con el bundle deben implementar la interfaz 
TideClockRegistrableInterface, es necesario podrá poderlas asociar con un usuario de reloj.  

Ejemplo:

```php

namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;
use Tide\TimeTideBundle\Entity\User as TimeTideUser;
use Tide\TimeTideBundle\Helpers\TideClockRegistrableInterface;

/**
 * @ORM\Entity()
 */
class Employee implements TideClockRegistrableInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

	/**
	 * @var string
	 * @ORM\Column(type="string")
	 */
    private $name;

	/**
	 * @var TimeTideUser
	 * @ORM\OneToOne(targetEntity="Tide\TimeTideBundle\Entity\User")
	 */
    private $timeTideUser;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getTimeTideUser(): ?TimeTideUser
    {
        return $this->timeTideUser;
    }

    public function setTimeTideUser(TimeTideUser $timeTideUser): self
    {
        $this->timeTideUser = $timeTideUser;

        return $this;
    }

    public function getDisplayName(): string {
    	return $this->getName();
    }

}

```

Imágenes de perfil
--------------------------------------
La obtención de imágenes del empleado se realiza con ayuda de un servicio que implemente la interfaz TimeTideUserImageProviderInterface.

El servicio debe agregarse al archivo de configuración services.yml
```yaml
services:
  App\Helpers\TimeTideUserImageProvider:
    arguments:
      - "@kernel"
      - "@doctrine.orm.default_entity_manager"
 ```      
 
 En este ejemplo las imágenes se encuentran en archivo jpg en una carpeta en la raíz del proyecto. 
 
 getProfileImage debe regresar un SplFileObject
 
```php
 use App\Entity\Employee;
 use Doctrine\ORM\EntityManagerInterface;
 use Symfony\Component\HttpKernel\KernelInterface;
 use Tide\TimeTideBundle\Entity\User;
 use Tide\TimeTideBundle\Helpers\TimeTideUserImageProviderInterface;
 
 class TimeTideUserImageProvider implements TimeTideUserImageProviderInterface {
 
 
 	/**
 	 * @var string $projectDir
 	 */
 	private $projectDir;
 
 	/**
 	 * @var EntityManagerInterface $em
 	 */
 	private $em;
 
 	public function __construct( KernelInterface $kernel, EntityManagerInterface $em ) {
 		$this->projectDir = $kernel->getProjectDir();
 		$this->em = $em;
 	}
 
 	function getProfileImage( User $user ): ?\SplFileObject {
 
 		//Look for user
 		$employeeRepo = $this->em->getRepository( 'App:Employee' );
 		$registrable = $employeeRepo->findOneBy( [ 'timeTideUser' => $user ] );
 
 		if(!$registrable){
 			$providerRepo = $this->em->getRepository('App:Provider');
 		    $registrable = $providerRepo->findOneBy(['timeTideUser'=>$user]);
 		}
 
 		$filesPath = $this->projectDir.'/employee_images';
 
 		try{
 			$profileFile = new \SplFileObject($filesPath.'/'.$registrable->getId().'.jpg', 'r');
 		}catch (\Exception $exception){
 			return null;
 		}
 
 		return $profileFile;
 	}
 }
```

Agregar y eliminar usuarios de relojes
----------------------------------------
Existe un servicio llamado ClockManager, en el se encuentran métodos que se encargan
de agregar y eliminar usuarios de relojes.

Ejemplo para agregar usuarios a un reloj desde un controlador:

```php
 use Tide\TimeTideBundle\Helpers\ClockManager;
 ...
 
 public function addEmployeeToClock(Request $request, ClockManager $clockManager){
     $data = json_decode($request->getContent(), true);
     $employee = $this->em->find('App:Employee', $data['employee']);
     $clock = $this->em->find('TimeTideBundle:Clock', $data['clock']);
     $clockManager->addUsersToClock($clock, [$employee]);
     return $this->json([], 204);
 }
 
```
addUsersToClock recibe un arreglo de entidades que implementan la interfaz TideClockRegistrableInterface.

Ejemplo para eliminar usuarios de reloj desde un controlador:
 
 ```php
  use Tide\TimeTideBundle\Helpers\ClockManager;
 ...
  public function removeClockUser(Request $request, ClockManager $clockManager){
 	    $employee = $this->em->find('App:Employee', $request->request->get('employee'));
 	    $clock = $this->em->find('TimeTideBundle:Clock', $request->request->get('clock'));
     	$clockManager->removeUsersFromClock($clock, [$employee]);
     	return $this->json([], 204);
     }
 
 ```
 
 Tip: Si se desea que al registrar un nuevo empleado o despedirlo se agregue o elimine del reloj se podría utilizar un EventListener
 que escuche el evento de persist del empleado y lo agregue o elimine de los relojes de la empresa a la que está asociado.



Obtener los registros de un usuario
----------------------------------------
Ejemplo de obtención de registros de un empleado.
```php
	public function getEmployeeRegisters(Request $request){
		$employee = $this->em->find('App:Employee', $request->query->get('employee'));
		$timeRegisters = $this->em->getRepository('TimeTideBundle:TimeRegister')->findBy(['user'=>$employee->getTimeTideUser()], ['date'=>'DESC']);
		return new Response($this->serialize($timeRegisters));
	}
```

