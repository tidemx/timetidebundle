<?php

namespace Tide\TimeTideBundle\Repository;

use Doctrine\ORM\EntityRepository;

class ReleaseRepository extends EntityRepository {

	public function findOrderedByAppVersion() {
		$qb = $this->createQueryBuilder( 'r' )
		           ->select( 'r' )
		           ->leftJoin( 'r.application', 'a' )
		           ->orderBy( 'a.name', 'ASC' )
		           ->addOrderBy( 'r.version', 'DESC' );

		return $qb->getQuery()->getResult();
	}


}