<?php

namespace Tide\TimeTideBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Tide\TimeTideBundle\Entity\Clock;

class ClockUserRepository extends EntityRepository {

	public function findUsersByClock( Clock $clock, $hydrationMode = Query::HYDRATE_ARRAY ) {
		$qb = $this->createQueryBuilder( 'cu' )
		           ->select( 'cu, u, c' )
		           ->leftJoin( 'cu.user', 'u' )
                   ->leftJoin( 'u.characteristics', 'c')
		           //->leftJoin( 'u.characteristics', 'c', 'WITH', 'c.sensorType = :sensorType' )
		           //->setParameter( 'sensorType', $clock->getSensorType() )
		           ->where( 'cu.clock = :clock' )
		           ->setParameter( 'clock', $clock );

		return $qb->getQuery()->getResult( $hydrationMode );
	}


}