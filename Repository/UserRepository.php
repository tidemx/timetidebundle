<?php

namespace Tide\TimeTideBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Tide\TimeTideBundle\Entity\Clock;
use Tide\TimeTideBundle\Entity\User;

class UserRepository extends EntityRepository{


	public function isUserInClock( User $user, Clock $clock ) {
		$result = $this->createQueryBuilder( 'u' )
		               ->select( 'u' )
		               ->leftJoin( 'u.clockUsers', 'cu' )
		               ->where( 'cu.clock = :clock' )->setParameter( 'clock', $clock )
		               ->andWhere( 'cu.user = :user' )->setParameter( 'user', $user )
		               ->getQuery()
		               ->getResult();
		if ( $result && count( $result ) > 0 ) {
			return true;
		}

		return false;
	}

	public function findUsersByClock( Clock $clock, $hydrationMode = Query::HYDRATE_ARRAY ) {
		$qb = $this->createQueryBuilder( 'u' )
		           ->select( 'u, c' )
		           ->leftJoin( 'u.clockUsers', 'cu' )
                   ->leftJoin( 'u.characteristics', 'c')
		           //->leftJoin( 'u.characteristics', 'c', 'WITH', 'c.sensorType = :sensorType' )
		           //->setParameter( 'sensorType', $clock->getSensorType() )
		           ->where( 'cu.clock = :clock' )
		           ->setParameter( 'clock', $clock );

		return $qb->getQuery()->getResult( $hydrationMode );
	}
}