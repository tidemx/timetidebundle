<?php
namespace Tide\TimeTideBundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Tide\TimeTideBundle\DependencyInjection\ImageProviderCompilerPass;

class TimeTideBundle extends Bundle {

	public function build(ContainerBuilder $container){
		parent::build($container);
		$container->addCompilerPass(new ImageProviderCompilerPass());
	}
}