<?php
namespace Tide\TimeTideBundle\EventSubscriber;
use Doctrine\Common\EventSubscriber;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Tide\TimeTideBundle\Entity\Characteristic;
use Tide\TimeTideBundle\Event\CharacteristicPostPersistEvent;
use Tide\TimeTideBundle\Event\CharacteristicPrePersistEvent;
use Doctrine\ORM\Event\LifecycleEventArgs;

final class CharacteristicSubscriber implements EventSubscriber
{
	/**
	 * @var EventDispatcherInterface $eventDispatcher
	 */
	private $eventDispatcher;

	public function __construct(EventDispatcherInterface $eventDispatcher)
	{
		$this->eventDispatcher = $eventDispatcher;
	}

	public function getSubscribedEvents()
	{
		return array(
			'prePersist',
			'postPersist'
		);
	}

	public function prePersist(LifecycleEventArgs $args)
	{
		/** @var Characteristic $characteristic*/
		$characteristic= $args->getObject();
		if($characteristic instanceof Characteristic){
		    $event = new CharacteristicPrePersistEvent($characteristic);
            $this->eventDispatcher->dispatch( $event, CharacteristicPrePersistEvent::NAME);
        }
	}

	public function postPersist(LifecycleEventArgs $args){
		/** @var Characteristic $characteristic */
		$characteristic = $args->getObject();
		if($characteristic instanceof  Characteristic){
			$event = new CharacteristicPostPersistEvent($characteristic);
			$this->eventDispatcher->dispatch( $event,CharacteristicPostPersistEvent::NAME);
		}
	}
}
