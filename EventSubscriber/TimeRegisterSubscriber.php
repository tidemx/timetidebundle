<?php
namespace Tide\TimeTideBundle\EventSubscriber;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Tide\TimeTideBundle\Entity\TimeRegister;
use Tide\TimeTideBundle\Event\TimeRegisterPostPersistEvent;
use Tide\TimeTideBundle\Event\TimeRegisterPrePersistEvent;

final class TimeRegisterSubscriber implements EventSubscriber
{
	/**
	 * @var EventDispatcherInterface $eventDispatcher
	 */
	private $eventDispatcher;

	public function __construct(EventDispatcherInterface $eventDispatcher)
	{
		$this->eventDispatcher = $eventDispatcher;
	}

	public function getSubscribedEvents()
	{
		return array(
			'prePersist',
			'postPersist'
		);
	}

	public function prePersist(LifecycleEventArgs $args)
	{
		/** @var TimeRegister $timeRegister */
		$timeRegister = $args->getObject();
		if($timeRegister instanceof TimeRegister){
		    $event = new TimeRegisterPrePersistEvent($timeRegister);
            $this->eventDispatcher->dispatch($event, TimeRegisterPrePersistEvent::NAME);
        }
	}

	public function postPersist(LifecycleEventArgs $args){
		/** @var TimeRegister $timeRegister */
		$timeRegister = $args->getObject();
		if($timeRegister instanceof  TimeRegister){
			$event = new TimeRegisterPostPersistEvent($timeRegister);
			$this->eventDispatcher->dispatch($event, TimeRegisterPostPersistEvent::NAME);
		}
	}
}
