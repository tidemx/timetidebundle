<?php
namespace Tide\TimeTideBundle\Exception;

use Exception;

/**
 * Triggered when Country entity related errors occur.
 */
class ClockLinkedException extends Exception
{

}