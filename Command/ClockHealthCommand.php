<?php

namespace Tide\TimeTideBundle\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Tide\TimeTideBundle\Entity\Clock;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ClockHealthCommand extends Command
{

    /**
     * @var EntityManagerInterface $em
     */
    private $em;

    protected static $defaultName = 'time-tide:check-clock-health';

    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct();
        $this->em = $em;
    }

    protected function configure()
    {
        $this
            ->setDescription('Notifies if a clock stop sending time registers');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        date_default_timezone_set('America/Mexico_City');

        $em = $this->em;
        $clocks = $em->getRepository('TimeTideBundle:Clock')->findAll();

        $now = new \DateTime();
        foreach ($clocks as $clock) {
            /**
             * @var Clock $clock
             */
            if ($clock->getTimeRegistersSyncDate()) {

                $t1 = strtotime($clock->getTimeRegistersSyncDate()->format('Y-m-d H:i'));
                $t2 = strtotime($now->format('Y-m-d H:i'));

                $minDiff = ($t2 - $t1) / 60;
                if ($minDiff > 240) {
                    throw new \Exception('Clock ' . $clock->getSerial() . ' has more than ' . $minDiff . ' minutes of inactivity');
                }
                echo $clock->getSerial() . ' - ' . $minDiff . " min\n";
            }
        }
        return Command::SUCCESS;
    }
}
