<?php

namespace Tide\TimeTideBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Tide\TimeTideBundle\Helpers\ClockManager;

class ResetClockApiKeyCommand extends Command
{
    protected static $defaultName = 'time-tide:reset-clock-api-key';

    private $clockManager;

    public function __construct(ClockManager $clockManager)
    {
        parent::__construct();
        $this->clockManager = $clockManager;
    }

    protected function configure()
    {
        $this
            ->setDescription('Reset Clock Api key so device can be linked again')
            ->addArgument('serial', InputArgument::REQUIRED, 'Clock serial');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $serial = $input->getArgument('serial');

        $clock = $this->clockManager->getClockBySerial($serial);

        if ($clock === null) {
            $io->error('Clock with serial ' . $serial . ' not found');
            return;
        }

        $this->clockManager->resetApiKey($clock);

        $io->success('Clock apikey reset complete');
        return Command::SUCCESS;
    }
}