<?php

namespace Tide\TimeTideBundle\Command;

use Symfony\Component\Console\Command\Command;
use Tide\TimeTideBundle\Entity\AssistanceId;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Tide\TimeTideBundle\Helpers\ClockManager;

class RegisterClockCommand extends Command
{
    protected static $defaultName = 'time-tide:register-clock';

    private $clockManager;

    public function __construct(ClockManager $clockManager)
    {
        parent::__construct();
        $this->clockManager = $clockManager;
    }

    protected function configure()
    {
        $this
            ->setDescription('Registers a new Tide clock')
            ->addArgument('name', InputArgument::REQUIRED, 'Clock name')
            ->addArgument('serial', InputArgument::REQUIRED, 'Clock serial');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $name = $input->getArgument('name');
        $serial = $input->getArgument('serial');

        $this->clockManager->createClock($name, $serial);

        $io->success('Clock created');

        return Command::SUCCESS;
    }
}
