<?php

namespace Tide\TimeTideBundle\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Style\SymfonyStyle;
use Tide\TimeTideBundle\Entity\Clock;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Tide\TimeTideBundle\Entity\ClockApplication;
use Tide\TimeTideBundle\Helpers\ApplicationManager;
use Tide\TimeTideBundle\Helpers\ClockManager;

class RegisterApplicationReleaseCommand  extends Command
{

    /**
     * @var EntityManagerInterface $em
     */
    private $em;

    /**
     * @var ApplicationManager
     */
    private $applicationManager;

    /**
     * @var ClockManager
     */
    private $clockManager;

    protected static $defaultName = 'time-tide:register-application-release';

    public function __construct(EntityManagerInterface $em, ApplicationManager $applicationManager, ClockManager $clockManager)
    {
        $this->em = $em;
        $this->applicationManager = $applicationManager;
        $this->clockManager = $clockManager;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->addArgument('zip_path', InputArgument::REQUIRED, 'Application release zip absolute path')
            ->setDescription('Register a new application release for clcoks')
            ->addOption(
                'install_in_all_clocks',
                null,
                InputOption::VALUE_OPTIONAL
            );
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $installInAllClocks = $input->getOption('install_in_all_clocks');


        $release = $this->applicationManager->addRelease($input->getArgument('zip_path'));

        if($installInAllClocks){
            $clocks = $this->em->getRepository('TimeTideBundle:Clock')->findAll();
            foreach ($clocks as $clock){
                $this->clockManager->addApplicationToClock($clock, $release, false);
            }
        }
        $this->em->flush();

        $io->success('App release registered');

        return Command::SUCCESS;
    }
}
